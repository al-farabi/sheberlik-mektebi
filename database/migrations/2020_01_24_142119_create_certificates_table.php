<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificatesTable extends Migration
{
 
    /**
     * Run the migrations.
     *
     * @return void 
     */
    public function up()
    {
        Schema::create('certificates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title'); 
            $table->string('image')->nullable(); 
            $table->boolean('published')->default(0);
            $table->text('short_text'); 
            $table->string('author')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('certificates');
    }

}
