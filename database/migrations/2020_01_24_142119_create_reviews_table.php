<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
 
    /**
     * Run the migrations.
     *
     * @return void 
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title'); 
            $table->string('image')->nullable(); 
            $table->boolean('published')->default(0);
            $table->text('short_text'); 
            $table->text('text'); 
            $table->string('author')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reviews');
    }

}
