@extends('layouts.app', ['title' => 'list'])
@section('content') 
<section class="banner">
	<div class="container">
		<div> 
			<h2 class="title">
				<span class="title_bkg">ОФОРМЛЕНИЕ</span>
				<span class="title_main">ОФОРМЛЕНИЕ ЗАКАЗА</span>
			</h2>
		</div>
	</div> 
</section>
<section class="banner"> 
	<div class="container">
		<div class="row"> 
		
			<div class="col-md-12"> 
				<h3>ШАГ 3 <span>Способ оплаты</span></h3>
			</div>
			
			<div class="col-md-6 tabcontent" style="display:block;padding:0;">  
				<div>
				<h3>Оплата по номеру карты kaspi</h3>
				<p>8962 5222 6663 2112</p>
				<p>Произведите оплату и укажите в комментарии название выбранного теста. Наш менеджер обработает ваш заказ в течении 8 часов и предоставит код активации для прохождения теста на ваш Email</p>
				</div>
				
				<a href="/quality" class="btn btn_color btn_border_gradient">
					<span class="title_gradien">Больше информации</span>
				</a>
				
			</div>
			<div class="col-md-6 tabcontent" style="display:block;padding:0;"> 
			
				<form method="POST" class="cart1" action="/api/login">
					<input type="hidden" name="_token" value="0MR3SgUnrCPxODxJ8Sr8q9SKEUpDEXBJmA2N2B5c">
					 
					<div class="row" style="padding:25px 0 0;">		
						<div class="col-md-6" style="padding:20px;"> 
						<h4>Выбранный тест</h4>
						<h2>Бойжеткенге тест</h2>
						</div>
						<div class="col-md-6" style="padding:20px;"> 
						<h4>Цена</h4>
						<h2>5 000</h2>
						</div>
					</div>	
			
				</form> 
				 
					<div class="row" style="padding:25px 0 0;">		
						<div class="col-md-6" style="padding:20px;">  
							<input type="text" name="text" placeholder="Код активации">
						</div>
						<div class="col-md-6" style="padding:20px;"> 
						
					 <a href="#" style="
    border: 1px solid #000;
    color: #000;" class="btn js-consultation">консультация</a>
					 
						</div>
					</div>	
				
			</div>
			
		</div>
	</div>
</section>
<section class="banner">
	<div class="container">
		<div class="row"> 
			<div class="col-md-12"> 
				<h3>ШАГ 4 <span>Платежная информация</span></h3> 
				
				<form class="cart2" method="POST" action="/api/feedback">
						<input type="hidden" name="user_id" value="1" />  
						<input type="hidden" name="title" value="test" />  
						<input type="hidden" name="message" value="test" />  
						
				<div class="row"> 
				<div class="col-md-4" style="
    padding-right: 25px;"> 
					<label> 
						<input type="text" name="name" placeholder="Ваше имя">  
					</label>
				</div>
				<div class="col-md-4" style="
    padding-right: 25px;"> 
					<label> 
						<input type="text" name="phone" placeholder="Контактный телефон">  
					</label>
				</div>
				<div class="col-md-4" style="
    padding-right: 25px;"> 
					<label> 
						<input type="text" name="email" placeholder="E-mail">  
					</label>
				</div>
				</div>
				
				
				
				<a href="/quality" class="btn btn_color btn_border_gradient">
					<span class="title_gradien">Больше информации</span>
				</a>
				
				<button class="btn btn_color btn_border_gradient_start">
					<span>Отправить</span>
				</button>
				
				</form>
				
			</div>
		</div>
	</div>
</section>
@include('includes.footer') 
@endsection