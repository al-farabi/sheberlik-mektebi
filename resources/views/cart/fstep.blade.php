@extends('layouts.app', ['title' => 'list'])
@section('content') 
<section class="banner">
	<div class="container">
		<div> 
			<h2 class="title">
				<span class="title_bkg">ОФОРМЛЕНИЕ</span>
				<span class="title_main">ОФОРМЛЕНИЕ ЗАКАЗА</span>
			</h2>
		</div>
	</div> 
</section>
<section class="banner"> 
	<div class="container">
		<div class="row"> 
			<div class="col-md-12"> 
				<h3>ШАГ 1 <span>Способ оформления заказа</span></h3>
			</div>
				 
				 
			<div class="col-md-6 tabcontent" style="display:block;padding:0;"> 
				<div>
				<h3>Регистрация Нового пользователя</h3>
				<p>Создание учетной записи поможет делать следующие покупки быстрее (не надо будет снова вводить адрес и контактную информацию), видеть состояние заказа, а также видеть заказы, сделанные ранее. 

Вы также сможете накапливать при покупках призовые баллы (на них тоже можно что-то купить), 
а постоянным покупателям мы предлагаем систему скидок.</p>
				</div>
				
				<a onclick="openCity(event, 'step-2')" class="btn btn_color btn_border_gradient">
					<span class="title_gradien">Продолжить</span>
				</a>
				
			</div>
			<div class="col-md-6 tabcontent" style="display:block;padding:0;"> 
			
				<form method="POST" class="cart1" action="/api/login">
					<input type="hidden" name="_token" value="0MR3SgUnrCPxODxJ8Sr8q9SKEUpDEXBJmA2N2B5c">
					<input type="hidden" name="redirect" value="/cart/2?step=2">  
					
					<h3 style="
    text-align: center;">
						<span class="title_main title_gradien">Вход</span>
					</h3>
					
			<div class="row" style="padding: 25px 0 0;">		
					<div class="col-md-6" style="
    padding: 20px;
"> 
              <input type="text" name="email" placeholder="Email">
            </div>
			
            <div class="col-md-6" style="
    padding: 20px;
"> 
              <input type="password" name="password" placeholder="Пароль">  
            </div>
			</div>	
			
			<div class="row" style="padding: 25px 0 0;">	
            <div class="col-md-6" style="
    padding: 20px;
"> 
                <input type="checkbox">
                <span class="checkmark"></span>
                <span>Запомнить</span>
            </div>
            <div class="col-md-6" style="
    padding: 20px;
"> 
              <button type="button" class="restore_pass">Забыли пароль?</button>
            </div>
            </div>
			
            <button type="submit" class="btn btn_color btn_border_gradient btn_logIn">
              <span class="title_gradien">Войти</span>
            </button>
          </form>
		   
			</div>
		</div>
	</div>
</section>
<section class="banner">
	<div class="container">
		<div class="row"> 
			<div class="col-md-12"> 
				<h3>ШАГ 2 <span>Личные данные для регистрации</span></h3> 
				
			<div class="tabcontent" id="step-2">
			
				<form method="POST" action="/api/register" class="cart2"> 
				<div class="row"> 
				<div class="col-md-4" style="
    padding-right: 25px;"> 
					<label> 
						<input type="text" name="name" placeholder="Ваше имя">  
					</label>
				</div>
				<div class="col-md-4" style="
    padding-right: 25px;"> 
					<label> 
						<input type="text" name="phone" placeholder="Контактный телефон">  
					</label>
				</div>
				<div class="col-md-4" style="
    padding-right: 25px;"> 
					<label> 
						<input type="text" name="email" placeholder="E-mail">  
					</label>
				</div>
				<div class="col-md-4" style="
    padding-right: 25px;"> 
					<label> 
						<input type="text" name="city" placeholder="Город">  
					</label>
				</div>
				<div class="col-md-4" style="
    padding-right: 25px;"> 
					<label> 
						<input type="text" name="position" placeholder="Должность">  
					</label>
				</div>
				<div class="col-md-4" style="
    padding-right: 25px;"> 
					<label> 
						<input type="text" name="organization" placeholder="Организация">  
					</label>
				</div>
				</div>
				
						<input type="hidden" name="password" value="1q2w3e4r">  
						<input type="hidden" name="password_confirmation" value="1q2w3e4r">  
						
				<button type="submit" class="btn btn_color btn_border_gradient">
					<span class="title_gradien">Продолжить</span>
				</button>
				
				</form>
				
			</div>
				
			</div>
		</div>
	</div>
</section>
@include('includes.footer') 
@endsection