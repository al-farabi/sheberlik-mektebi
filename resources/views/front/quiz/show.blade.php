@extends('layouts.app', ['title' => 'list'])
@section('content')
<section class="banner">
	<div class="container">
 
	</div>
</section>
<section class="map">
	<div class="container">
	
		<h1 style="margin-left:380px; color:white;">{{ $questionnaire->title }}</h1>
		<h3 style="margin-left:380px; color:white;">{{ $questionnaire->description}}</h3>

		{!! Form::open(array('action' => 'ResponseController@index', 'id' => 'saveanswer')) !!}
		@if (isset ($question))
			 
            @foreach ($question as $answer)
                    <tr style="font-size:18px; background-color:white">
                      <td>{{ $answer->title }}</td>
                      <td>
                    
                
                      </td>
            @endforeach
				  
		@endif
        {!! Form::submit('Submit', ['class' => 'btn btn-success']) !!} 
        {!! Form::close() !!}
		
	</div>
</section>
 
      <div class="modal-content" style="padding-top:20px;">
          <div class="container">
            <div class="row"> 
            <!--  !! Form::open(array('action' => 'AnswersController@store', 'id' => 'saveanswer')) !!  -->
              <table class="table table-striped table-bordered">
                @if (isset ($question))
                <thead>
                  <tr style="background-color:#2a363b; color:white;">
                    <td style="width: 500px;"><h3>Question</h3></td>
                    <td><h3>Options</h3></td>
                  </tr>
                </thead>
                <div>
                  @foreach ($question as $answer)
                    <tr style="font-size:18px; background-color:white">
                      <td>{{ $answer->content }}</td>
                      <td>
           
                      </td>
                  @endforeach
                    </tr>
                </table>
                <div  class="buttons2">
                  <td><a href="{{ url('/response') }}" class="btn btn-danger">Cancel</a></td>
                  <td>
				  
                    <!-- {!! Form::submit('Submit', ['class' => 'btn btn-success']) !!} -->
                    <!-- {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!} -->
                    <!-- {!! Form::close() !!} -->
					
                  </td>
                @else
                  <p> No questions available </p>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        @include('includes.footer')
      </footer>
@endsection
