@extends('layouts.app', ['title' => 'list'])
@section('content')
<section class="banner">
	<div class="container">
 
	</div>
</section>
<section class="article">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-3">
				<div class="lcircle">
					<img src="/images/profil.jpg"> 
				</div>
			</div>
			<div class="col-md-9">
				<div class="card"> 
				<div class="card-body">
					<div>
					<h1>Джон Голланд</h1>
					<p>{{ $questionnaire->title }} Не следует, однако забывать, что дальнейшее развитие различных форм деятельности требуют от нас анализа позиций, занимаемых участниками в отношении поставленных задач. Повседневная практика показывает, что начало повседневной работы по формированию позиции требуют определения и уточнения существенных финансовых и административных условий.</p>
					</div>
					<div>
					<strong>Особенности реализации:</strong>
					<p>
полноценная полная клиническая методика – возможно, впервые в интернете;
цвета подобраны максимально близко к оригинальным цветовым таблицам;
определяется уровень тревожности, указаны возможные внутренние конфликты;
доступен полный протокол исследования и подробная разметка выборов;
для каждого результата формируется короткая ссылка, которой можно поделиться;
совершенно бесплатно и анонимно, регистрация не требуется.</p>
					</div>
				</div>

					<div class="card-body">
						<a class="btn btn-dark" href="/questionnaires/{{ $questionnaire->id }}/questions/create">Add New Question</a>
						<a class="btn btn-dark" href="/surveys/{{ $questionnaire->id }}-{{ Str::slug($questionnaire->title) }}">Take Survey</a>
			
			@php
			$i = 0;  
			@endphp
			
			<div onclick="openCity(event, 'answer-{{ $i }}')" class="tabcontent btn btn_color btn_border_gradient_start" style="display:block;">
				<span>Пройти тестирование</span>
			</div>
			
			<form action="/surveys/{{ $questionnaire->id }}-{{ Str::slug($questionnaire->title) }}" method="POST">
			<input type="hidden" name="questionnaire_id" value="{{ $questionnaire->id }}" />
			
			@foreach($questionnaire->questions as $key => $question) 
			@if ($i==0)
			<div id="answer-{{ $i }}" class="tabcontent">
			@else         
			<div id="answer-{{ $i }}" class="tabcontent">
			@endif 	
			
				<div>{{ $question->question }}</div>
                <div>
				{{ $i++ }}
				@foreach($question->answers as $answer)
				
					<label onclick="openCity(event, 'answer-{{ $i }}')"><input type="radio" name="responses[{{ $key }}][answer_id]" value="{{ $answer->id }}">{{ $answer->answer }}</label>
				
				@endforeach
                <input type="hidden" name="responses[{{ $key }}][question_id]" value="{{ $question->id }}">
				</div>
			</div> 
			@endforeach
			 
			<div id="answer-{{ $i }}" class="tabcontent">
				<button type="submit">Завершить</button>
			</div> 
			
			</form>
			
					</div>
			</div> 
       
			</div>
		</div>
	</div>
</section>
@include('includes.footer')
@endsection
