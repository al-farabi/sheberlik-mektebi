<?php
use Illuminate\Http\Request;
 
Route::post('api/login', function (Request $request) {  
    
    if (auth()->attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
           
        $user = auth()->user(); 
		 
        $user->remember_token = Str::random(60);
		$user->save(); 		
		 
		if($request->input('redirect')) {
		 
        return response()->json([
            'message' => 'redirect',
            'url' => $request->input('redirect'),
        ]);
		
		}else{
		
        return response()->json([
            'message' => 200,
        ]);
		
		}
    }
    
    return response()->json([
        'error' => 'Unauthenticated user',
        'code' => 401,
    ], 401);
});

Route::any('api/register', 'Auth\RegisterController@postRegister');
Route::post('api/feedback', 'FeedbackController@create');
 

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');

 
/*
Route::get('/', function () {
    return view('welcome');
});
*/


Route::post('/answer/create', 'AnswersController@create');

Route::group(['prefix' => 'api'], function() { 

Route::resource('/answer', 'AnswersController', ['only' => [
   'create',
]]);
	 
});

 
Route::get('about', function() {
		return view('front.about');
	})->name('about');

Route::get('quality', function() {
		return view('front.about');
	})->name('quality');

Route::get('contact', function() {
		return view('front.contact');
	})->name('contact');
 
Route::get('articles', function() {
		return view('front.articles');
	})->name('articles');
	
Route::get('/services/', 'ServiceController@list')->name('service.list');
Route::get('/services/{id}', 'ServiceController@detail')->name('service.detail');

Auth::routes();

/**/
Route::get('/cart/thanks', 'CartController@thanks');
Route::get('/cart/{slug}', 'CartController@step');
/**/

Route::get('/questionnaires/create', 'QuestionnaireController@create');
Route::post('/questionnaires', 'QuestionnaireController@store');
Route::post('/questionnaires/finish', 'QuestionnaireController@finish');
Route::get('/questionnaires/{questionnaire}', 'QuestionnaireController@show');

Route::get('/questionnaires/{questionnaire}/questions/create', 'QuestionController@create');
Route::post('/questionnaires/{questionnaire}/questions', 'QuestionController@store');
Route::delete('/questionnaires/{questionnaire}/questions/{question}', 'QuestionController@destroy');

Route::get('/surveys/{questionnaire}-{slug}', 'SurveyController@show');
Route::post('/surveys/{questionnaire}-{slug}', 'SurveyController@store');

 
    Route::get('/response', 'ResponseController@index');
    Route::resource('/response', 'ResponseController');
    Route::resource('/completed', 'CompletedController');
 