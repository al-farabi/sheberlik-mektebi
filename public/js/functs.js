/*
function postAjax(url, data, success) {
    var params = typeof data == 'string' ? data : Object.keys(data).map(
            function(k){ return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) }
        ).join('&');

    var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhr.open('POST', url);
    xhr.onreadystatechange = function() {
        if (xhr.readyState>3 && xhr.status==200) { success(xhr.responseText); }
    };
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send(params);
    return xhr;
}

// example request
postAjax('http://quiz/api/login', 'name=Denis&email=vsyakiyj@mail.ru&password=1q2w3e4r&password_confirmation=1q2w3e4r', function(data){ console.log(data); });
*/ 
  
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
} 

//Отправка форм посредство ajax
function submitFormWithAjax(btn_or_form, callback){ 

    if($(btn_or_form).prop("tagName") == "FORM"){
        $form = $(btn_or_form);
    }else{
        $form = $(btn_or_form).closest("form");
    } 
	
    data = $form.serialize();
    action = $form.attr("action");
	  
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $form.children('input[name="_token"]').val()
		},
		url: action,
		type: 'POST',                
		data: data,
		success: callback,
		error: function(e) {

			console.log("ERROR : ", e);

		}
	}); 
}

//Отправка форм посредство post
function submitFormWithPost(btn_or_form, callback){ 
	 
    if($(btn_or_form).prop("tagName") == "FORM"){
        $form = $(btn_or_form);
    }else{
        $form = $(btn_or_form).closest("form");
    } 
	
    data = $form.serialize();
    action = $form.attr("action"); 
 
    $.post(action, data, callback);
}

    $("form").submit(function (e) { 
	
        e.preventDefault(); 
        e.stopPropagation();   
		     
        submitFormWithPost(this, function (json) { 
			if(json.message=='created') {
				
				window.location='/';
				
			}		
			if(json.message=='404') {
				
				alert('Не найден');
				
			}		
			if(json.message=='redirect') {
				
				window.location=json.url;
				
			}	
			if(json.message=='200') {
				
				window.location='/';
				
			}	 
        });
		
    });