(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

var _articleSlider = _interopRequireDefault(require("./modules/article-slider"));

var _feedbackSlider = _interopRequireDefault(require("./modules/feedback-slider"));

var _schoolSlider = _interopRequireDefault(require("./modules/school-slider"));

var _ficationSlider = _interopRequireDefault(require("./modules/fication-slider"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// You can write a call and import your functions in this file.
//
// This file will be compiled into app.js and will not be minified.
// Feel free with using ES6 here.
// import dotsEffect from './modules/dots';
(function ($) {
  $(function () {
    _articleSlider["default"].init();

    _feedbackSlider["default"].init();

    _schoolSlider["default"].init();

    _ficationSlider["default"].init(); // trigger dom
    // burger trigger


    $('.menu').on('click', function () {
      $(this).toggleClass('active');
      $('body').toggleClass('freez');
      $('.header_item.header_right').toggleClass('show');
    }); // burger trigger end
    // popup auth

    $('.popup_auth_wrapper').first().addClass('show');
    $('.js-Buy').on('click', function () {
      event.preventDefault();
      $('body').addClass('freez');
      $('.popup_mask').fadeIn();
      $('.popup_Buy').addClass('show');
    });
    $('.js-logIn').on('click', function () {
      event.preventDefault();
      $('body').addClass('freez');
      $('.popup_mask').fadeIn();
      $('.popup_logIn').addClass('show');
    });
    $('.js-reg').on('click', function () {
      event.preventDefault();
      $('body').addClass('freez');
      $('.popup_logIn').removeClass('show');
      $('.popup_signUp').addClass('show');
    });
    $('.js-log').on('click', function () {
      event.preventDefault();
      $('body').addClass('freez');
      $('.popup_signUp').removeClass('show');
      $('.popup_logIn').addClass('show');
    });
    $('.close_popup').on('click', function () {
      event.preventDefault();
      $('body').removeClass('freez');
      $('.popup_Buy').removeClass('show');
      $('.popup_signUp').removeClass('show');
      $('.popup_logIn').removeClass('show');
      $('.popup_mask').fadeOut();

      if ($(window).width() < 1200) {
        $('body').addClass('freez');
      }
    });
    $('.popup_mask').on('click', function () {
      $('body').removeClass('freez');
      $('.popup_Buy').removeClass('show');
      $('.popup_signUp').removeClass('show');
      $('.popup_logIn').removeClass('show');
      $('.popup_mask').fadeOut();

      if ($(window).width() < 1200) {
        $('body').addClass('freez');
      }
    }); // popup auth end
    // header fixed

    $(window).scroll(function () {
      if ($(window).scrollTop() > 0) {
        $('.header').addClass('scroll');
      } else {
        $('.header').removeClass('scroll');
      }
    }); // heade fixed end
    // window resize

    $(window).resize(function () {
      if ($(window).width() >= 1201) {
        $('*').removeClass('active');
        $('*').removeClass('freez');
        $('*').removeClass('show');
        $('.popup_mask').fadeOut();
      }

      if ($(window).width() < 1200) {
        $('.popup_mask').on('click', function () {
          $('body').removeClass('freez');
        });
        $('.close_popup').on('click', function () {
          $('body').removeClass('freez');
        });
      }
    });
  });
})(jQuery);

},{"./modules/article-slider":2,"./modules/feedback-slider":3,"./modules/fication-slider":4,"./modules/school-slider":5}],2:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var articleSliderWrapper = function () {
  var articleSliderInit = function articleSliderInit() {
    var carousel;

    if (carousel === undefined) {
      carousel = new Swiper($('.swiper-container.article_slider'), {
        direction: 'horizontal',
        // slidesPerView: 3.5,
        loop: true,
        autoplay: true,
        autoplaySpeed: true,
        spaceBetween: 30,
        breakpoints: {
          1570: {
            slidesPerView: 4.5,
            spaceBetween: 30
          },
          999: {
            slidesPerView: 3.5,
            spaceBetween: 30
          },
          930: {
            slidesPerView: 2.5,
            spaceBetween: 15
          },
          520: {
            slidesPerView: 2.2,
            spaceBetween: 15
          },
          320: {
            slidesPerView: 1.5,
            spaceBetween: 15
          }
        }
      });
    }
  };

  var init = function init() {
    articleSliderInit();
  };

  return {
    init: init
  };
}();

var _default = articleSliderWrapper;
exports["default"] = _default;

},{}],3:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var feedbackSliderWrapper = function () {
  var feedbackSliderInit = function feedbackSliderInit() {
    var carousel;

    if (carousel === undefined) {
      carousel = new Swiper($('.swiper-container.feedback_slider'), {
        direction: 'horizontal',
        slidesPerView: 'auto',
        loop: true,
        // autoplay: true,
        // autoplaySpeed: true,
        // spaceBetween: 60,
        // centeredSlides: true,
        breakpoints: {
          1100: {
            slidesPerView: 3,
            spaceBetween: 60
          },
          768: {
            slidesPerView: 2,
            spaceBetween: 30
          },
          320: {
            slidesPerView: 1,
            spaceBetween: 15
          }
        }
      });
    }
  };

  var init = function init() {
    feedbackSliderInit();
  };

  return {
    init: init
  };
}();

var _default = feedbackSliderWrapper;
exports["default"] = _default;

},{}],4:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var schoolSliderWrapper = function () {
  var schoolSliderInit = function schoolSliderInit() {
    var carousel;

    if (carousel === undefined) {
      var _ref;

      carousel = new Swiper($('.swiper-container.fication_slider'), (_ref = {
        direction: 'horizontal',
        slidesPerView: 'auto'
      }, _defineProperty(_ref, "slidesPerView", 3), _defineProperty(_ref, "loop", true), _defineProperty(_ref, "spaceBetween", 60), _defineProperty(_ref, "breakpoints", {
        950: {
          slidesPerView: 3,
          spaceBetween: 60
        },
        600: {
          slidesPerView: 2,
          spaceBetween: 30
        },
        320: {
          slidesPerView: 1,
          spaceBetween: 15
        }
      }), _ref));
    }
  };

  var init = function init() {
    schoolSliderInit();
  };

  return {
    init: init
  };
}();

var _default = schoolSliderWrapper;
exports["default"] = _default;

},{}],5:[function(require,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var schoolSliderWrapper = function () {
  var schoolSliderInit = function schoolSliderInit() {
    var carousel;

    if (carousel === undefined) {
      var _ref;

      carousel = new Swiper($('.swiper-container.school_slider'), (_ref = {
        direction: 'horizontal',
        slidesPerView: 'auto'
      }, _defineProperty(_ref, "slidesPerView", 3), _defineProperty(_ref, "loop", true), _defineProperty(_ref, "spaceBetween", 60), _defineProperty(_ref, "breakpoints", {
        900: {
          slidesPerView: 3,
          spaceBetween: 60
        },
        600: {
          slidesPerView: 2,
          spaceBetween: 15
        },
        320: {
          slidesPerView: 1,
          spaceBetween: 15
        }
      }), _ref));
    }
  };

  var init = function init() {
    schoolSliderInit();
  };

  return {
    init: init
  };
}();

var _default = schoolSliderWrapper;
exports["default"] = _default;

},{}]},{},[1]);


