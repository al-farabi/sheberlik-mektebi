<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Feedback2 extends Model
{ 
    protected $table = 'feedback_user';
	 
    protected $fillable = [
        'title', 
        'message',  
        'created_at',
        'updated_at'
    ];
	
    public function scopeLast($query)
    {
        $query->orderBy('date', 'desc')->limit(4);
    }
	
}
