<?php 

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Codes extends Model
{

    protected $table = 'codes';

    protected $fillable = [
        'codes', 
    ];

    public function scopeLast($query)
    {
        $query->orderBy('date', 'desc')->limit(4);
    }
 
}
