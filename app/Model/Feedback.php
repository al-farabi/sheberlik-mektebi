<?php 

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{  
    protected $table = 'feedback_user';

    protected $fillable = [
        'user_id', 
        'title', 
        'message',  
        'created_at',
        'updated_at'
    ];

    public function scopeLast($query)
    {
        $query->orderBy('date', 'desc')->limit(4);
    }

}
