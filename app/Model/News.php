<?php 

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{

    protected $table = 'news';

    protected $fillable = [
        'title',
        'url',
        'image',
        'date',
        'published',
        'text',
        'short_text',
        'author',
        'created_at',
        'updated_at'
    ];

    public function scopeLast($query)
    {
        $query->orderBy('date', 'desc')->limit(4);
    }
 
}
