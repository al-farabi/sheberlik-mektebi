<?php 

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Certificates extends Model
{  
    protected $table = 'certificates';

    protected $fillable = [
        'title', 
        'image', 
        'published',  
        'short_text',
        'author',
        'created_at',
        'updated_at'
    ];

    public function scopeLast($query)
    {
        $query->orderBy('date', 'desc')->limit(4);
    }

}
