<?php 

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Options extends Model
{

    protected $table = 'options';

    protected $fillable = [
        'codes', 
    ];

    public function scopeLast($query)
    {
        $query->orderBy('date', 'desc')->limit(4);
    }

}
