<?php 

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Reviews extends Model
{  
    protected $table = 'reviews';

    protected $fillable = [
        'title', 
        'image', 
        'published', 
        'text',
        'short_text',
        'author',
        'created_at',
        'updated_at'
    ];

    public function scopeLast($query)
    {
        $query->orderBy('date', 'desc')->limit(4);
    }

}
