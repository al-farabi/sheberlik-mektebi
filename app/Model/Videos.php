<?php 

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Videos extends Model
{ 
    protected $table = 'videos';

    protected $fillable = [
        'title',
        'url',
        'video', 
        'published', 
        'short_text',
        'author',
        'created_at',
        'updated_at' 
    ];

    public function scopeLast($query)
    {
        $query->orderBy('date', 'desc')->limit(4);
    }

}
