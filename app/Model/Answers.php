<?php 

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Answers extends Model
{

    protected $table = 'answers';

    protected $fillable = [
        'codes', 
    ];

    public function scopeLast($query)
    {
        $query->orderBy('date', 'desc')->limit(4);
    } 

}
