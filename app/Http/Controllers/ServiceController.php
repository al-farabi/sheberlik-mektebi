<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\questionnaires;
use App\questions;
 

class ServiceController extends Controller
{ 

	public function __construct(){
		//$this->middleware('auth');
		//parent::__construct();
	}
  
    public function index(Request $request)
    {
        $data = array();

        return Response::json($data);
    } 
	
    public function list()
    {  
        $questionnaires = questionnaires::all();
		
        return view('services/list', ['questionnaires' => $questionnaires]);
    } 
	
    public function detail($id)
    {  	
          $questionnaire = questionnaires::where('id', $id)->first();   
          $questions = questions::where('questionnaires_id', $questionnaire->id)->get();
          // $questionnaire['questions'] = $question;
          if(!$questionnaire)
          {
              return redirect('/'); // you could add on here the flash messaging of article does not exist.
          }
          return view('services/detail')->with('questionnaire', $questionnaire)->with('questions', $questions);
    } 
}