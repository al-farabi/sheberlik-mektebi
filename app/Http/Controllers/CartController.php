<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Questionnaire;
use App\Question;

use Auth;

class CartController extends Controller
{
    public function __construct() {
		 
    }
 
    public function step() {  
		 
		if (Auth::check()) {

			return view('cart.sstep');  
		}else{

			return view('cart.fstep');  
		}	 
    }
	
	public function thanks() {
		 
		return view('cart.thanks');  
	}
}
