<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Videos;
use App\Model\Certificates;

class HomeController extends Controller
{
    
    public function __construct()
    {
        //$this->middleware('auth');
    }  
	 
    public function index()
    {
		$sarray = array();
		$varray = array();
		
		/*
		$user = auth()->user(); 
		print_r($user); 
		die();
		*/
		
        $videos = Videos::all();   
        $certs = Certificates::all();   
        //published
		 
		foreach($videos as $video) {   
			//echo Img::get_video_key('dfsds');
			
			$varray[] = array (
				'title'=>$video['title'],
				'img'=>self::view_video_preview($video['url']) 
			);
		}  
		  
		foreach($certs as $cert) {    
			
			$sarray[] = array (
				'title'=>$cert['title'],
				'img'=>$cert['image']
			);
		}  
		
        return view('welcome', ['videos'=>$varray, 'certs'=>$sarray]);
    }
	
	
	
	
	public function get_video_key($url) {
    /*
     * Получаем уникальный код клипа с Youtube
     */
    $Query = parse_url($url, PHP_URL_QUERY);
    if($Query) {
        $Rows = explode('&', $Query);
        foreach($Rows as $part) {
            $Arguments = explode('=', $part);
            if($Arguments[0] == 'v') {
                return $Arguments[1];
            }
        }
    }
    return False;
}
public function view_video_preview($url, $mod='default') {
    /*
     * Возвращаем ссылку на скрин
     * $mod = [default, 0, 1, 2, 3]
     * где $mod = 0 большой скрин
     */
    $video_key = self::get_video_key($url);
    return "http://img.youtube.com/vi/{$video_key}/{$mod}.jpg";
}
 
}
