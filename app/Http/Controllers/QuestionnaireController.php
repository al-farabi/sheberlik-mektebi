<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
 
use App\Questionnaire;
use App\Question;
use App\Answer;
use App\Model\Codes;
 
use Auth;

class QuestionnaireController extends Controller
{
    public function __construct() {
		
		//$this->middleware('auth');
		 
    }

    public function create() {
        return view('questionnaire.create');
    }

    public function store(Questionnaire $questionnaire, Request $request) {  
    //public function store() { 
	
		/*
        $data = request()->validate([
            'title' => 'required',
            'purpose' => 'required'
        ]);
		*/
        //$questionnaire = auth()->user()->questionnaire()->create($data);
        //$questionnaire2 = $questionnaire->questions()->create($request);
		
		$data = array(
			'user_id'=>1,
			'title'=>$request->title,
			'purpose'=>$request->purpose 
		);
		
        $questionnaire2 = $questionnaire->create($data);

        return redirect('/questionnaires/'.$questionnaire2->id);
    }

    public function show(\App\Questionnaire $questionnaire) {
		  
		if (Auth::check() && Codes::where('questionnaire_id', $questionnaire['id'])
					->where('user_id', Auth::user()->id)
					->first())
		{
			
			$questionnaire->load('questions.answers.responses');
			
			//dd($questionnaire);
			 
			return view('questionnaire.show', compact('questionnaire'));
			
		}else{
			
			return redirect()->action(
				'CartController@step', ['slug' => $questionnaire['title']]
			);
		} 
    }
	
    public function answer() {
		
	} 
}
