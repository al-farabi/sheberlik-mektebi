<?php

namespace App\Http\Controllers;

use App\Questionnaire;

class SurveyController extends Controller
{ 
    public function __construct() {		
	
		$this->middleware('auth');		
		
    }
	
    public function show(Questionnaire $questionnaire, $slug) {
        // dd($questionnaire);

        $questionnaire->load('questions.answers');

        return view('survey.show', compact('questionnaire'));
    }

    public function store(Questionnaire $questionnaire) { 
         
        $data = request()->validate([
            'responses.*.answer_id' => 'required',
            'responses.*.question_id' => 'required'  
        ]);
        // dd($data);
		
		$data['survey']['name'] = 'test'; 
		$data['survey']['email'] = 'test@test.kz'; 
		 		 
        $survey = $questionnaire->surveys()->create($data['survey']);
        $survey->responses()->createMany($data['responses']);

        return response()->json(array(
			'message'=>'redirect',
			'url'=>'/services',
			'error'=>'false'
		));
    }
}
