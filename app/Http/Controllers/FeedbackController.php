<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\Feedback;

class FeedbackController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }  
	
    public function index()
    {
		
	}
	
    public function store() 
	{
	
	}
	
    public function create(Feedback $feedback, Request $request)
    {  
		
		$data = array( 
			'user_id'=>$request->input('user_id'),
			'title'=>$request->input('title'),
			'message'=>$request->input('message')
		);
		
		$feedback->create($data); 
		 
        return response()->json([
            'message' => 'redirect',
            'url' => 'thanks',
        ]);
	}
}
