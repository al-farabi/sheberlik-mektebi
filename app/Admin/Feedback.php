<?php
  
use App\Model\Feedback; 
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Feedback::class, function (ModelConfiguration $model) { 
    $model->setTitle('Заявки');
    
    $model->onDisplay(function () {
        $display = AdminDisplay::table()->setColumns([
		
            AdminColumn::text('id')->setLabel('#'),
            AdminColumn::text('title')->setLabel('Название'),
            AdminColumn::text('url')->setLabel('url'),
            AdminColumn::image('image')->setLabel('Изображение')->setWidth('180px'),
            AdminColumn::text('text')->setLabel('Описание'),
     
            //AdminColumn::text('updated_at')->setLabel('Дата изменения'),
        ]);
        $display->setApply(function ($query) {
            $query->orderBy('id', 'asc');
        });
        $display->paginate(25); 
        return $display;
    });

    $model->onCreateAndEdit(function () {

        $form = AdminForm::panel();

        $form->addBody([
		
            AdminFormElement::text('title', 'Название')->required()->unique(), 
            AdminFormElement::image('image', 'Главное изображение'), 
            AdminFormElement::wysiwyg('short_text', 'Короткое описание')->required(), 
            AdminFormElement::wysiwyg('text', 'Описание')->required(), 
            AdminFormElement::text('author', 'Автор') 
			
        ]);
        return $form;
    });
})->addMenuPage(Feedback::class, 4)
    ->setIcon('fa fa-shopping-cart');