<?php
  
use App\Model\Videos;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Videos::class, function (ModelConfiguration $model) { 
    $model->setTitle('Видео');
    
    $model->onDisplay(function () {
        $display = AdminDisplay::table()->setColumns([
		
            AdminColumn::text('id')->setLabel('#'),
            AdminColumn::text('title')->setLabel('Название'),
            AdminColumn::text('url')->setLabel('Ссылка на видео'),  
     
            //AdminColumn::text('updated_at')->setLabel('Дата изменения'),
        ]);
        $display->setApply(function ($query) {
            $query->orderBy('id', 'asc');
        });
        $display->paginate(25);
        return $display;
    });

    $model->onCreateAndEdit(function () {

        $form = AdminForm::panel();

        $form->addBody([
		
            AdminFormElement::text('title', 'Название')->required()->unique(),
            AdminFormElement::text('url', 'Ссылка на видео')->required()->unique(),
            AdminFormElement::file('video', 'Видео'), 
            AdminFormElement::wysiwyg('short_text', 'Короткое описание') 
			
        ]);
        return $form;
    });
})->addMenuPage(Videos::class, 4)
    ->setIcon('fa fa-shopping-cart');