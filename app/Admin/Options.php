<?php
  
use App\Model\Options;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Options::class, function (ModelConfiguration $model) { 
    $model->setTitle('Вопросы');
    
    $model->onDisplay(function () {
        $display = AdminDisplay::table()->setColumns([
		
            AdminColumn::text('id')->setLabel('#'),
            AdminColumn::text('codes')->setLabel('Код') 
     
            //AdminColumn::text('updated_at')->setLabel('Дата изменения'),
        ]);
        $display->setApply(function ($query) {
            $query->orderBy('id', 'asc');
        });
        $display->paginate(25);
        return $display;
    });

    $model->onCreateAndEdit(function () {

        $form = AdminForm::panel();

        $form->addBody([
            AdminFormElement::text('title', 'Код')->required()->unique(), 
        ]);
        return $form;
    });
})->addMenuPage(Options::class, 4)
    ->setIcon('fa fa-shopping-cart');
	 