<?php
  
use App\Model\Codes;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(Codes::class, function (ModelConfiguration $model) { 
    $model->setTitle('Коды');
    
    $model->onDisplay(function () {
        $display = AdminDisplay::table()->setColumns([
		
            AdminColumn::text('id')->setLabel('#'),
            AdminColumn::text('codes')->setLabel('Код') 
     
            //AdminColumn::text('updated_at')->setLabel('Дата изменения'),
        ]);
        $display->setApply(function ($query) {
            $query->orderBy('id', 'asc');
        });
        $display->paginate(25);
        return $display; 
    });

    $model->onCreateAndEdit(function () {

        $form = AdminForm::panel();

        $form->addBody([
            AdminFormElement::text('user_id', 'user_id'), 
			AdminFormElement::custom()
                    ->setDisplay(function($instance) {
                        return
                            '<input class="form-control" type="hidden" id="user_id" name="user_id" value="0">';
                    }),
            AdminFormElement::text('questionnaire_id', 'Номер теста')->required(), 
            AdminFormElement::text('codes', 'Код')->required()->unique(), 
			AdminFormElement::custom()
                    ->setDisplay(function() {
			return
                '<button class="btn btn-primary">Сгенерировать код</button>';
                    }),
        ]);
        return $form;
    });
})->addMenuPage(Codes::class, 4)
    ->setIcon('fa fa-shopping-cart');
	 