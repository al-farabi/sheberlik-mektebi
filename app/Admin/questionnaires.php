<?php
  
use App\questionnaires;
use SleepingOwl\Admin\Model\ModelConfiguration;

AdminSection::registerModel(questionnaires::class, function (ModelConfiguration $model) { 
    $model->setTitle('Тесты');
    
    $model->onDisplay(function () {
        $display = AdminDisplay::table()->setActions([
		
			AdminColumn::action('export', 'Export')->setIcon('fa fa-share')->setAction('/services'),
			
		])->setColumns([
		
            AdminColumn::text('id')->setLabel('#'),
            AdminColumn::text('title')->setLabel('Название'),
			AdminColumn::text('updated_at')->setLabel('Дата изменения')
			
        ]);
		
        $display->setApply(function ($query) {
            $query->orderBy('id', 'asc');
        });
		
        $display->paginate(25);
        return $display;
    });

    $model->onCreateAndEdit(function () {

        $form = AdminForm::panel();

        $form->addBody([
		
            AdminFormElement::text('user_id', 'user_id'), 
			AdminFormElement::custom()
                    ->setDisplay(function($instance) {
                        return
                            '<input class="form-control" type="hidden" id="user_id" name="user_id" value="0">';
                    }),
			
            AdminFormElement::text('title', 'Название')->required()->unique(), 
            AdminFormElement::wysiwyg('purpose', 'Описание')->required()->unique()
			
        ]);
        return $form;
    });
})->addMenuPage(questionnaires::class, 4)
    ->setIcon('fa fa-shopping-cart');
	 